# 기초전산유체역학 
   - Introduction to Computational Fluid Dynamics
   - ASE4011

## 강의 목표
   - 전산유체역학 기초 이론 학습 및 원리 이해
   - 수치해석 기법을 이용한 편미분 방정식 해석
   - 오픈소스 유동해석 프로그램 [pyBaram](https://aadl_inha.gitlab.io/pyBaram/) 을 이용하여 실제 비행체 주위 유동 해석 과정을 실습

## 선수과목
   - 저속공기역학, 고속공기역학
   - 컴퓨터 프로그래밍, 수치해석

## 유의사항
   - 프로그래밍은 Python으로 진행됩니다. 기본적인 Python 사용법은 1주차에 소개하지만, [Scipy Lecture Note](https://scipy-lectures.org/) 참고 바랍니다.
      * [Python 특징](https://colab.research.google.com/drive/1USw1ZzyQlrXJatvfv_kPiyyhj8uYUVmO)
      * [Python 문법](https://colab.research.google.com/drive/1n-JBTkq5wCD_mEHI4qDJ1-tXXoRrTDAe)
      * [Numpy 소개](https://colab.research.google.com/drive/16_90Pz5b4-MI0ttUxTCkQMcRJBiR2L4N)
   - 매 수업은 강의 후 실습을 진행합니다.

## 주차별 강의 내용 
   - 1주차 - 전산유체역학 소개, Python 소개
   - 2주차 - 수치 미분 개요
   - 3주차 - Wave Equation (Upwind vs Central)
   - 4주차 - Consistency, Stability, Convergence
   - 5주차 - Semi-discrete form
   - 6주차 - Laplace Equation
   - 7주차 - Iterative Method to solve linear system
   - 8주차 - 중간고사
   - 9주차 - Finite Volume method
   - 10주차 - Burgers equation (First order vs Second order)   
   - 11주차 - pyBaram 소개 및 설치
   - 12주차 - 익형 주위 비점성 유동 해석
   - 13주차 - 초음속 흡입구 유동 해석
   - 14주차 - 점성 경계층 유동 해석
   - 15주차 - 기말고사
